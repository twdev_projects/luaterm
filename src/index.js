import 'xterm/css/xterm.css';

import {Terminal} from 'xterm';

import LuaModule from './lua.js';

const term = new Terminal({
  cursorBlink : true,
  convertEol : true,
});

term.open(document.getElementById('xterm-container'));

let moduleArgs = {
  pending_fgets : [],
  pending_chars : [],
  pending_lines : [],

  print : (text) => {
    console.log(text);
    term.write(text + '\n');
  },

  printErr : (text) => {
    console.error(text);
    term.write('\x1b[31m' + text + '\x1b[0m' +
               '\n');
  }
};

term.onData((data) => {
  const isEnter = data === '\r';
  const isBackspace = data === '\x7f';

  if (isEnter) {
    moduleArgs.pending_lines.push(moduleArgs.pending_chars.join(''));
    moduleArgs.pending_chars = [];
    term.write('\r\n');
  } else if (isBackspace) {
    moduleArgs.pending_chars.pop();
    term.write('\b \b');
  } else {
    moduleArgs.pending_chars.push(data);
    term.write(data);
  }

  if (moduleArgs.pending_fgets.length > 0 &&
      moduleArgs.pending_lines.length > 0) {
    let resolver = moduleArgs.pending_fgets.shift();
    resolver(moduleArgs.pending_lines.shift());
  }
});

console.log(LuaModule);

// start wasm module
LuaModule(moduleArgs).then((mod) => { window.mod = mod; });
