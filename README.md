# Lua + xterm.js integration

This small projects integrates Lua built as WASM bytecode and
xterm.js.

## Building

    git clone --recurse-submodules https://gitlab.com/twdev_projects/luaterm
    npm install
    make
    npm run serve

emscripten toolchain is required.

Demo is available [here](https://luaterm-twdev-projects-8f27b3e697f168462164aedef4949bee5125ab8e.gitlab.io/).
