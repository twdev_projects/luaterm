all: index.html

index.html: src/index.js src/lua.js
	npm run build

src/lua.js:
	emmake $(MAKE) -C lua -j
	cp lua/lua.js src/lua.js
	cp lua/lua.wasm src/lua.wasm

clean:
	rm -rf dist
	$(MAKE) -C lua clean
	rm -fv src/lua.js src/lua.wasm
	rm -fv lua/lua.js lua/lua.wasm
