const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  mode : 'development',
  entry : {
    index : './src/index.js',
  },
  output : {
    filename : '[name].bundle.js',
    path : path.resolve(__dirname, 'dist'),
    clean : true,
  },
  plugins :
          [
            new HtmlWebpackPlugin(
                {title : 'Lua + xterm.js REPL', inject : 'body', minify : true}),
          ],
  devtool : 'inline-source-map',
  devServer : {
    static : './dist',
  },
  module : {
    rules : [ {test : /\.css$/, use : [ 'style-loader', 'css-loader' ]} ]
  },
};
